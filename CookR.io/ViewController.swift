//
//  ViewController.swift
//  CookR.io
//
//  Created by Martello Jones on 2017-03-21.
//  Copyright © 2017 Konsolt.Beyond Technology Corporation. All rights reserved.
//

import UIKit
import CookROrderProcessing

class ViewController: UIViewController {

    @IBAction func startOrderFlow(_ sender: Any) {
        if let flowController = OrderFlowInitiator.flowController(with: nil) {
            self.present(flowController, animated: true)
        }
    }
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.flowResult(notification:)), name: NSNotification.Name(rawValue:"flow.result"), object: nil)
        
        super.viewDidLoad()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func flowResult(notification result:Notification){
        print("notication.object = \(String(describing: result.object))")
        
        let controller = UIAlertController(title: "Processing Result", message: "User selected: \(result.object as! String)", preferredStyle: .alert)
        
        self.present(controller, animated: false, completion: {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3, execute: {
                controller.dismiss(animated: true, completion: nil)
            })
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

