//
//  OrderFlowController.swift
//  Pods
//
//  Created by Martello Jones on 2017-04-21.
//
//

import UIKit

class OrderFlowController: UIViewController {
    var selectedFlavour:String?
    let flavours = ["Sweet Burger-Burger", "Smokey Mint Heaven", "Chilly Cheezed Chilli", "Gray. Just Gray"]
    
    @IBAction func chooseFlavor(_ sender: Any) {
        let controller  = UIAlertController(title: "Meal flavour", message: "Choose your meal flavour", preferredStyle: .actionSheet)
        
        let actionHandler = { (action:UIAlertAction) in
            print("Action = \(action)")
            self.selectedFlavour = action.title
        }
        
        for flavour in self.flavours {
            let action = UIAlertAction(title: flavour, style: .default, handler: actionHandler)
            controller.addAction(action)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        
        self.present(controller, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedFlavour = self.flavours[0]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Segue: \(segue)")
        
        if segue.identifier == "completeOrder" {
            if let completeOrderController = segue.destination as? OrderCompleteController {
                completeOrderController.set(flavour: self.selectedFlavour)
            }
        }
    }

}
