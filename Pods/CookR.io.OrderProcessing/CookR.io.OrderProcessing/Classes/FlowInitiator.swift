//
//  FlowInitiator.swift
//  Pods
//
//  Created by Martello Jones on 2017-04-21.
//
//

import Foundation

protocol FlowInitiator {
    func create(with data:Dictionary<String, Any>?) -> UIViewController?
}
