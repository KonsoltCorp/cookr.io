import Foundation
import UIKit


open class OrderCompleteController: UIViewController {
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    var selectedFlavour:String?
    
    func set(flavour selected:String?) {
        self.selectedFlavour = selected
    }
    
    @IBAction func completeOrderFlow(_ sender: Any) {
        print("Order flow complete!")
        self.navigationController?.dismiss(animated: true, completion: nil)
        
        // deliver the outcome of the flow back to whichever module (or main app) that initiated the flow
        var flowResult = Notification(name: Notification.Name(rawValue: "flow.result"))
        flowResult.object = self.selectedFlavour
        
        NotificationCenter.default.post(flowResult)
    }
}
