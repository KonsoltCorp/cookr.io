//
//  OrderFlowInitiator.swift
//  Pods
//
//  Created by Martello Jones on 2017-04-21.
//
//

import Foundation

open class OrderFlowInitiator: FlowInitiator {
    // factory method for instantiating a flow controller
    public class func flowController(with parameters: Dictionary<String, Any>?) -> UIViewController? {
        return OrderFlowInitiator().create(with:parameters)
    }
    
    // internal implementation that conforms to the flow initiator protocol
    // ideally FlowInitiator would be a public protocol that can be shared across multiple modules.
    // for the purpose of this example, declaring it inside this module is sufficient
    func create(with data: Dictionary<String, Any>? = nil) -> UIViewController? {
        print("OrderFlowInitiator: start flow")
        
        // get the framework bundle that this class belongs to
        let frameworkBundle = Bundle(for: OrderFlowInitiator.self)
        
        // use framework bundle to lookup the resource bundle that our module's resources will be 
        // packaged in (see the s.resource_bundles mapping in podspec)
        let bundlePath = frameworkBundle.path(forResource: "CookROrderProcessing", ofType: "bundle")
        let resourceBundle = Bundle(path: bundlePath!)
        
        // with the correct bundle, create a storyboard and return the initial controller
        return UIStoryboard(name: "OrderProcessing", bundle: resourceBundle).instantiateInitialViewController()
    }
}
